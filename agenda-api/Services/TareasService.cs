using System.Collections.Generic;
using agenda_api.Model;
using Microsoft.AspNetCore.Mvc;

namespace agenda_api.Services
{
    
    public class TareasService : ControllerBase
    {
        public List<Tarea> GetTareas()
        {
            return new List<Tarea>()
            {
                new Tarea(){ Titulo = "Tara A", Descripcion = "Descripcion A"},
                new Tarea(){ Titulo = "Tara B", Descripcion = "Descripcion B"},
                new Tarea(){ Titulo = "Tara C", Descripcion = "Descripcion C"},
                new Tarea(){ Titulo = "Tara D", Descripcion = "Descripcion D"}
            };
        }
    }
}