namespace agenda_api.Model
{
    public class Tarea
    {
        public string Titulo { get; set; }

        public string Descripcion { get; set; }
    }
}