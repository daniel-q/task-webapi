using agenda_api.Services;
using Microsoft.AspNetCore.Mvc;

namespace agenda_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TareasController: ControllerBase
    {
        private TareasService tareasService;

        public TareasController()
        {
            tareasService = new TareasService();
        }

        [HttpGet]
        public IActionResult Tareas()
        {
            return Ok(tareasService.GetTareas());
        }
    }
}